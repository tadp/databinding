// global functions to run examples
define(['jquery','databind/src/rendering'],function($, r){
	
	function bindCallbacks(){
		var span, lis = $( '#exampleList > li' );
		for(var i= 0, l = lis.length; i < l; i++){
			var h = $('h3', lis[i])[0] || lis[i];
			span = document.createElement( 'SPAN' );
			span.innerHTML = 
				'<a id="e'+ (i+1) +'" class="run"  href="#">Run</a>';
			h.parentNode.insertBefore(span, h.nextSibling);
		}
		//use event delegation
		$(document).delegate('#exampleList .run', 'click', function(e){
			run(this, window[this.id]);
			return false;
		});
	}

	// run all tests at once
	var runAll = function(a){
		a.onclick = function(){return false;};
		var links = $('#exampleList .run');
		
		for(var i= 0, l= links.length; i< l; i++){
			var li= links[i];
			run(li, window[li.id]);
		}
	},

	// run a test
	run = function(elm, ob, debug){
		if(!elm) return;
		elm.innerHTML = 'Show Source';
		$(elm).click(function(){
			showSource(ob, this);
			return false;
		});
		transform(ob, debug);
	};
	function showSource(o, a){
		var old = document.getElementById('sourceCodes'),
			txtShow = 'Show Source',
			txtHide = 'Hide Source';
		if(a.hasSource){
			a.innerHTML = txtShow;
			a.hasSource=false;
			$(old).remove();
			return;
		}
		var li = $('li.' + a.id + ' div.template')[0],
			src = document.createElement('DIV'),
			srcNb = 0,
			addSrc = function(title, source){
				srcNb++;
				var t = document.createElement('DIV'),
					tt = document.createElement('DIV');
				t.className = 'sourceTitle';
				t.innerHTML = title;
				tt.className = 'sourceCode';
				tt.innerHTML = '<pre>'+source+'</pre>';
				tt.insertBefore(t, tt.firstChild);
				src.appendChild(tt);
			};
		if(old){
			$('a', old.parentNode)[0].innerHTML = txtShow;
			old.parentNode.removeChild(old);
		}
		src.id = 'sourceCodes';
		if(typeof o === 'function'){
			addSrc('Function', o.toString());
		}else{
			o.template && addSrc('HTML', o.original.replace(/\</g,'&lt;').replace(/\>/g,'&gt;').replace(/\t/g, '  '));
			o.directive && addSrc('Directive', JSON.stringify(o.directive, null, 2));
			o.data && addSrc('Data', JSON.stringify(o.data, null, 2));
		};
		src.className = 'cols' + srcNb;
		li.parentNode.insertBefore(src, li);
		a.innerHTML = txtHide;
		a.hasSource=true;
	};
	// run a transformation
	var transform = function(ob, debug){
		
		if(typeof ob === 'function')
			return ob(r);
		
		var templates= r(ob.template),
			result;
		
		//keep a copy of the template
		var dv = document.createElement('DIV');
		dv.appendChild((templates[0]).cloneNode(true));
		ob.original = dv.innerHTML;
		
		
		if(ob.directive) //using directives
			result= templates.render(ob.data, ob.directive, ob.autorender);
		else 	// auto-interpolation
			result= templates.render(ob.data);
		
		if(ob.doublerender)
			result= r(result[0]).render(ob.data, ob.directive2);
			
		$(ob.template).replaceWith(result[0]);	
		
		//to render using a directive AND auto-interpolation, you can either:
		//with render(): pass 'true' in arg3, eg. render(directive, data, true)
		//with compile(): pass 'data' to the function, eg. compile(directive, data)

	};
	bindCallbacks();
	
});
