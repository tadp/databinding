// examples data
var e1 = {
	template:'.e1 .template',
	data:{ greeting: 'Hello', 
		   who: 'World!',
		   message:'Binds to "value" attribute'
		}
};
var e2= {
	template: '.e2 .template',
	data: {
		activities: [
			{   date: '2012-12-02',
				activity: 'UI design',
				comment: 'Main interface',
				name: 'John Stucke'
			}, 
			{	date: '2012-12-20',
				activity: 'Data API',
				comment: 'Design data API',
				name: 'Alex May'
			  }
		]}
};
var e3= {
	template: '.e3 .template',
	data: {
		tags: ['python ','django ', 'programming']
	}
};
var e4= {
	template: '.e4 .template',
	data: {
	  title:    'Hello World',
	  post:     'Hi there it is me',
	  comments: [ {
		  name: 'John',
		  text: 'That rules'
		}, {
		  name: 'Arnold',
		  text: 'Great post!'
		}
	  ]
	}
};
var e5 = {
	template:'.e5 .template',
	data:{ 
		firstname: 'John',
		lastname:  'Nobody',
		address: {
			street: '4th Street',
			city:   'San Francisco',
			zip:    '94199'
		}		
	}
};

var e6 = {
	template:'div.hello',
	data:{ who:'Hello World!' },
	directive:{ 'span.who':'who' }
};

var e7 = {
	template:'div.friends',
	directive:{
		'.who':'who.name',
		'.who@title':'See the tweets of #{who.twitter}',
		'.who@href+':'who.twitter'
	},
	data:{
		people:[
			{	name:'Morpheus', 
				twitter:'morpheus'
			},
			{	name:'Trinity', 
				twitter:'trinity'
			}
		], 
		who:{name:'Neo', 
			twitter:'username'
		}
	},
	autorender: true
};

var e8 = {
	template:'table.playerList',
	directive:{
		'tbody tr':{
			'player<-players':{
				'@class+': styleEvenOdd,
				'td':'player',
				'td@style': 'cursor:pointer'
			}
		}
	},
	data:{
		players:[
			"Adrian Meador","Michèle Noïjû", "Γέφυρα γρύλων","Chloé Ellemême","глобальных масштабах","יוצא לשוק העולמי"
		]
	}
};
function styleEvenOdd(ctx){ //receives context object
	//ctx => {context:data, items:items, pos:pos, item:items[pos]};
	return (ctx.pos % 2 == 0) ? 'even' : 'odd';
}


var e9 = {
	template:'table.partialTable',
	data:{
		cols:['name', 'food', 'legs'],
		animals:[
			{name:'bird', food:'seed', legs:2},
			{name:'cat', food:'mouse, bird', legs:4},
			{name:'dog', food:'bone', legs:4},
			{name:'mouse', food:'cheese', legs:4}
		]
	},
	directive:{
		'th':{
			'col<-cols':{
				'.':'col'
			}
		},
		'td':{
			'col<-cols':{
				'@class':'col'
			}
		}
	},
	directive2:{'tbody tr':{
		'animal<-animals':{
			'td.name':'animal.name',
			'td.food':'animal.food',
			'td.legs':'animal.legs'
		}
	}},
	doublerender:true
};

var e10 = {
	template:'div.scoreBoard',
	data:{
		teams: [{
			name: 'Cats',
			players: [	
				{first: 'Alicé', last: 'Kea\'sler', score: [16, 15, 99, 100]}, 
				{first: '', name: '', score: 0},
				{first: 'Vicky', last: 'Benoit', score: [3, 5]}, 
				{first: 'Wayne', last: 'Dartt', score: [9, 10]}
			]
		},{	name: 'Dogs',
			players: [
				{first: 'Ray', last: 'Braun', score: 14}, 
				{first: 'Aaron', last: 'Ben', score: 24}, 
				{first: 'Steven', last: 'Smith', score: 1}, 
				{first: 'Kim', last: 'Caffey', score: 19}
			]
		},{ //note that 'players' is null! so, nothing is rendered. See my notes in loopNode(), in rendering.js
			name:'Birds',
			players:null
			
		},{	name: 'Mice',
			players: [
				{first: 'Natalie', last: 'Kinney', score: 16}, 
				{first: 'Caren', last: 'Cohen', score: 3}
			]
		}]
	},
	directive:{
		'tr.scoreBoard': {
			'team <- teams': {
				'td.teamName' : 'team.name',
				'tr.teamList': {
					'player <- team.players': {
						'td.player': '#{player.first} (#{player.last})',
						'td.score': '#{player.score}',
						'td.position': 
							function(ctx){
								return ctx.pos + 1;
						},
						'@class+': styleEvenOdd 
					}
				}
			}
		}
	}
};

var e11 = {
	template:'.e11 form',
	directive:{
        '.sizes option': {
            'size <- sizes': {
                '.':'#{size.val} - #{size.name}',
				'@value':'size.val',
                '@selected':'size.sel'
            }
        },
        '.colors option': {
            'color <- colors': {
                '.':'color.name',
				'@value':'color.val'
            }
        },
        //WARNING: to select an option in a <select>, make sure that the select's <option>s have been rendered already. Otherwise, the engine can't find the options to select. That's why I put this rule AFTER the rule that renders the <option>s for colors. 
        'select.colors': 'G'
	},
	data:{
		sizes: [{ val:'S', name: 'small' }, { val:'M', name: 'medium', sel: true }, {val:'L', name:'large'}],
		colors: [{val:'B', name:'blue'}, {val:'Y', name:'yellow'},{val:'G', name:'green'},{val:'O', name:'orange'}]
	}
};


/*
var e7 = {
	template:'ul.treeItem',
	data:{
		children: [{
			name: 'Europe',
			children: [{
				name: 'Belgium',
				children: [{
					name: 'Brussels',
					children:null},{
					name: 'Namur'},{
					name: 'Antwerpen'}]},{
				name: 'Germany'},{
				name: 'UK'}]},{
			name: 'America',
			children: [{
				name: 'US',
				children: [{
					name: 'Alabama'},{
					name: 'Georgia'}]},{
				name: 'Canada'},{
				name: 'Argentina'}]},{
			name: 'Asia'},{
			name: 'Africa'},{
			name: 'Antarctica'}
		]
	},
	directive:{
		'li': {
			'child <- children': {
				'a': 'child.name',
				'a@onclick':'alert(\'#{child.name}\');',
				'div.children': function(ctxt){
					return ctxt.child.item.children ? ex07.rfn(ctxt.child.item):'';
				}
			}
		}
	}
}; */

