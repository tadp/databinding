//This module needs Array.isArray, make sure to load the fallback ( 'lib/lang/array'). Fallbacks must be loaded in a global dependencies module, because it's ugly to have to load it in each file I need Array.isArray!! so I could load a common deps module in "myapp.js" (not here), to *patch* the native objects such as Array. why? because these features I need are *standard* Javascript (eg. isArray), not *additional* features.

define(['./directive_parser','./databind','jquery', 'events/bean'],function(parse, db, $, _){
		
	function readData(path, data, member){
		
		// in a loop, when arrayName is not given (eg. "product<-") or it's "context" (eg. "product<-context"), loopSpec.arrayName is undefined, so 'path' is undefined
		if(!path)	return data.context || data; //why added " || data"? because 'data' is not always a tempCtxt, so the property 'context' might not be found, in that case return original data.
				
		var m = path.split('.'),
			v = data[m[0]],
			i = 0;
		// what does the following code block do? Answer: see in loopNode(), in innerLoop(). The arg 'data' passed to readData() is not always the actual data, in loops it's the 'tempCtxt' object. If actionSpec has a name to identify the item in an array, eg. "product.price", m[0] would be the itemName, and it can be retrieved from tempCtxt. tempCtxt[itemName], which returns a 'loopCtxt' object, which has the properties 'items', 'item', 'pos'. In property 'item' is stored the *actual* data for the current item in the iteration. That's why, if('v.item') is not undefined is a sign that 'v' is a loopCtxt object. By contrast, if we are not inside a loop => arg 'data' is not a tempCtxt, and thus data[m[0]] will return actual data object or a value, not a loopCtxt object.    
		if(v && v.item){
			i += 1;
			if(m[i] === 'pos'){
				return v.pos;
			}else{
				data = v.item;
			}
		// what does the next line do? when a sel inside a loop points to a datamember outside the data item in array, the data to look in is data['context'], which contains the orig data object. otherwise, the datamember won't be found in the data object you return.
		}else if(!v && data.context){
			data=data.context;
		}
		var n = m.length;
		for(; i < n; i++){
			if(!data){break;}
			//Added this if-statement to return the object and property name for path. I need it in databinding funcs to make observable the datamember.
			if(member && i== (n-1)){
				return [data, m[i]];
			}
			data = data[m[i]];
		}
		// when data is not found, it returns 'undefined'. 
		return data;
	}
	
	//helper function to select options/checkboxes/radios using array of values to select
	function setSelectedValues(node, prop, vals){
		var opts= node.options || $('input',node);
		if(!Array.isArray(vals)) vals=[vals];
	    
		if(node.multiple==false){
	       node.value=vals[0];
	       return;
	    }
		//using indexOf() allows us to define values in vals[] in any arbitrary order, ie. the values given in vals[] don't need to be sorted following <option>s order within <select>. This is important because we can't force the app developer to sort the values passed in vals[]
		for(var i=0,l=opts.length; i<l;i++){
			var opt= opts[i];
			if(vals.indexOf(opt.value)>-1)    
	           opt[prop]=true;
			else
	           opt[prop]=false;
	    } 
	}
	
	
	var loopNode = function(node, loopDef, data, state, tmplIndex){  //	loopDef = {	filter, sort, loopSpec, directive };
		var arrayName= loopDef.loopSpec.arrayName;
		// Warning: arrayName could be 'undefined'. when? when the directive for loop did not say data to loop over (eg. "product<-") or said 'context' (eg. "product<-context").

		var dfrag = document.createDocumentFragment(),
			items = readData( arrayName, data ); //returns an array
			
		if(!items){
			//If 'items' is 'null' then return. Note, "!items" gives true only when 'items' is null/undefined, not when 'items' is an empty array, because if it's empty array I mustn't return, coz i need to bind the array to the node, in case items are inserted into the array at later time. 
			//Edit: since we don't render anything, the default values in the sub-template will be displayed in the result HTML (eg. see example 10, in which data for 'birds' team has players=null). but it'd be better if we *cleared* those default values. So, the easiest solution is to remove the associated 'node' from the DOM:
			node.parentNode.removeChild(node);
			return;
			}
		var	il = items.length,
			parentNode= node.parentNode;  //parent to attach cloned nodes.			
					
		//pass the loopDef.directive to transform() to process child definitions. loopDef.directive is a map of selectors-actions, like the original directive. Also pass the data associated to loopDef.loopSpec.arrayName, ie. items[]
		var	tempCtxt = { context:data },
		loopCtxt = tempCtxt[ loopDef.loopSpec.itemName ] = {},
		innerLoop = function(tempCtxt, loopCtxt, item, node_i, pos_i){
			//for each entry prepare the parameters for function directives, and sub templates
			tempCtxt.item = loopCtxt.item = item;
			tempCtxt.node = loopCtxt.node = node_i;
			tempCtxt.pos  = loopCtxt.pos  = pos_i;
			
			dfrag.appendChild( transform( node_i, tempCtxt, loopDef.directive, state, tmplIndex ) );
								
		},
		pos = 0;
		tempCtxt.items = loopCtxt.items = items;
		
		if( Array.isArray(items) ){
			for( ; pos < il; pos++ ){
				innerLoop(tempCtxt, loopCtxt, items[pos], node.cloneNode(true), pos);
			}
		}else{ // else: it's an object with *named* properties, not an array of items
			for(pos in items){
				if(items.hasOwnProperty(pos))
				innerLoop(tempCtxt, loopCtxt, items[pos], node.cloneNode(true), pos);
			}
		}
		//when append a docfrag to DOM, all its child nodes are appended automatically
		if(dfrag.childNodes.length)
			parentNode.replaceChild( dfrag, node );	
		//if the data array was empty, nothing to be rendered in this loop-> remove the node used as template for rendering items in array 
		else
			parentNode.innerHTML='';
			
		//------- Listen to events on array insert/remove for 2-way data-binding -------
		//-To observe changes on Arrays, I expand the array object with custom funcs., those in the module ObservableArray.js. 
		state.push([items,db.watchArray]);	
		
		//Note that I can only observe add/remove events on *Arrays*, but not on custom objects. ie. if you add (or delete) a property to an existing object, I can't detect such event!

		state.push([items, 'add',function(from, to, elms){
		
			//TODO: 2) handle when a loop array does not contain objects but strings (or numbers), eg. people:['peter','john',...], instead of people:[{name:'peter'},{name:'john'},...]
			
			//TODO: WARNING: innerLoop() is going to append new nodes to dfrag, make sure that dfrag is empty!! if i didn't clone dfrag when i appended it to DOM (some lines above), then it'll be empty, otherwise i'll have to empty it
			
			
			//get data item(s) inserted into array and build tempCtxt{}.
			for(var i=0;i< elms.length;i++){ //'elms' is inserted items[]
				innerLoop(tempCtxt, loopCtxt, elms[i], node.cloneNode(true), pos++);
								
				//XXX: innerLoop() calls transform(), which renders the sub-template. transform() returns a node, which is appended to the DocFragment, coz at this time the docFragment is empty. 
			}
			//insert into DOM at the index given in the trigger event. However, to simplify this func, at the moment I *append* the node to the parent, ignoring the index at which the item was inserted in the data array. 
			//var newNodes= dfrag.childNodes;
			//BUG: the above line caused a bug, coz newNodes is a NodeList, not an Array. That means that when dfrag is appendded to parentNode, those nodes are not child nodes of dfrag anymore, so the NodeList is empty. Thus, when I trigger 'DOMNodeAdded', newNodes is an *empty* NodeList!. solution: store the child nodes in an Array, not a NodeList.
			var newNodes= [], n= dfrag.firstChild;
			for( ; n; n= n.nextSibling)
				newNodes.push(n);
			
			
			//FIXED: node.parentNode is null. why? coz 'node' was in template, but since html was rendered and replaced 'node', 'node' lost reference to its parent. Use instance of parentNode stored in vble
			//node.parentNode.appendChild(dfrag);
			parentNode.appendChild(dfrag);
						
			//notify other modules that a new node has been inserted into the DOM, eg. Timesheets.js module listens to this event to re-evaluate the timesheet definitions. Warning: I get dfrag.childNodes *before* dfrag is appened to DOM, coz it'll be empty after that.
			//TODO: should i hide the new node? because if Timesheets module decides it is not displayed, there could be an instant in which the node is drawn in the HTML, other ideas to avoid it?? 
			$(document).trigger('DOMNodeAdded', [newNodes ,parentNode]);
		
			//bind each new data item with the new DOM node: 	
			// after rendering the new content using transform(), i need to bind the new data with the new rendered html. how? pass to transform() an empty queue[] in its last arg, and after transform() returns the result, go thru the funcs in queue[] and call them to do the databinding. [don't need to create a new queue[] array, coz it should be emptied after we did the global databinding for the view. so, remember to call queue.clear() everytime after we call the callbacks in queue[]].
			state.processQueue();

			
		},db.tempEvtBind]);

		state.push([items,'remove',function(from, to, elms){
			pos-= (to - from +1);
			//get data item(s) removed from array and delete the associated DOM nodes
			//$("#parent").children().slice(N).remove();
			//The .detach() method is the same as .remove(), except that .detach() keeps all jQuery data associated with the removed elements.
			var nodes= $(parentNode).children().slice(from,to+1).detach(); //'to' is endIndex that was deleted, I sum '+1' because slice() does not include last index in the result.
	
			//notify other modules that a new node has been removed from the DOM, eg. Timesheets.js module listens to this event to re-evaluate the timesheet definitions.
			//WARNING: do not use event name "DOMNodeRemoved", because that's a native event
			$(document).trigger('DOMNodeDeleted', [nodes,parentNode]); //DA: in jQuery's trigger(), if the data passed is an array, jquery maps each item in the array to a different arg in the callback that listens for this event. So, to let the callback receive all the data in one arg only, I have to put 'data' as one item inside an array (or object), eg. '[data]' when 'data' is an array.
						
			//update nextSiblings' binding data, ie. the index of data item, why? because when removed an item in data array, the indexes in node's binding don't point to the correct item! 
			//Only need this when elements in array are single values, not objects. why? if elements are objects, the tuple stored in node does not have an index, what it stores is the object itself, ie. tuple is (item_obj, property) instead of (array, item_index). Also, if items[] is empty (ie. !items.length) I don't need to update the indexes either.
			if(!this.length || typeof this[0]==="object") return; //'this' is items[]
			
			var tuple, dif= to - from +1,
				nextNode= parentNode.childNodes[from];
			while(nextNode){
				tuple= $.data(nextNode,'databind');
				if(tuple) tuple[1]-= dif;
				nextNode= nextNode.nextSibling;
			}
						
		}, db.tempEvtBind]);
		
		
	};
	/* in getAction(), consider special cases when setting input values/attrs. If node is:
	- <textarea>: if selSpec says to set its content, use node.value for that. edit: it's ok to use text() to set a value, but not to get its value when the user edited the textarea. [if user edited the textarea, jquery's text() doesn't get the current value, it only gets the initial value.] 
	- <input>, type= checkbox/radio, and selSpec.attr="checked", call node.checked instead of node.attr('checked', value), and test if value in actionSpec is false, otherwise assume it's true (eg. value "checked" or "" will set node.checked=true)
	- <option>, and selSpec.attr="selected", same thing as with 'checked'. call node.selected instead of node.attr("selected", value). 
	- the same issue happens with other attrs of <input>, eg. "disabled", thus I should keep a hashmap with all those attrs that need to be set thru JS instead of as inline attr.  
	- <select>, and selSpec.attr="value", since this is not standard HTML, use setSelectedValues() to be able to set <select>'s selected options, either one value or an array of values.
	- <fieldset>, same thing as with <select>. <fieldset> will have <input> nodes with type "checkbox" (multiple selection) or "radio" (can select only one item). */
	var boolAttrs={
		'checked': function(node){
					return /^(checkbox|radio)$/.test(node.type);	
				},
		'selected': function(node, tag){
					return tag==="option";	
				},
		'disabled':function(node, tag){
					return /^(input|select)$/.test(tag);	
				}
		//TODO: add other properties that need special treatment, eg. "required" attr ??
		};					
	var getAction= function(node, selSpec, actionSpec){
		var set, val,
			append= selSpec.append,
			prepend= selSpec.prepend;
		//Warning: append/prepend is problem when user can edit the value, eg. in a text field, if data was appended to its value, and then the user edits the text, and then the bound data in model changes, we cant detect what text in the input field to replace with new data. what should we do in such case? Idea1: constraint the developer to not use append/prepend on user-editable values. Edit: i fixed this issue following Idea1-- if you define append/prepend on an editable element, the system automatically won't bind data to the node, ie. if data changes after html rendered it won't update the UI.

		//to get attribute, use jQuery's attr() to handle differences with IE (eg. with style,...)
		if(selSpec.attr){
			var attName = selSpec.attr, tagName= node.nodeName.toLowerCase();
			
			//FIXED: if what we append/prepend is a 'class' attr, we should add a leading space, otherwise the developer has to remember to add the leading space in the directive, and i want to avoid it. Eg. if a directive defines: 
			// "@class+":'even' 
			//...and the node has already a class "myclass", the result 'class' attr will be "myclasseven", which is not the desired result, the result should be "myclass even". I don't want to force the developer to add leading space in directives, ie. ' even' instead of 'even'. Thus, here I detect if attName is 'class', and if so add leading space to value. If append/prepend, always append to 'class' attr, no need to prepend, because it makes no difference in this case.
			if(attName==="class"){
				if(append || prepend){
					set = function($node, neu, old){
						$node[0].className= $node[0].className +' '+neu; 
					};									
				}else{
					set = function($node, neu, old){  
						$node[0].className= neu; 
					};	
				}
			}
			
			
			else if(append){
				set = function($node, neu, old){ 
					$node.attr(attName, $node.attr(attName) + neu); 
				};
			}else if(prepend){
				set = function($node, neu, old){
					$node.attr(attName, neu + $node.attr(attName) ); 
				};
			}else{
				var boolAttr= boolAttrs[attName];
				if(boolAttr && boolAttr(node, tagName)){
					set = function($node, neu, old){ 
						$node[0][attName]= !!neu; // '!!neu' gives true/false 
					};
				}else if(attName=="value" && (tagName=="select" || tagName=="fieldset")){
					if(tagName=="fieldset") attName="checked";
					else if(tagName=="select") attName="selected";
					
					set = function($node, neu, old){ 
						setSelectedValues($node[0],attName, neu);
					};				
				}else{
					set = function($node, neu, old){ 
						$node.attr(attName, neu ); 
					};
				}
			}
		}else{ //else: what it sets is the node's contents, not an attribute
			if (append){
				set = function($node, neu, old) { 
					$node.text($node.text() + neu);	
				};
			}else if(prepend) {
				set = function($node, neu, old) { 
					$node.text(neu + $node.text());	
				};
			} else {
				set = function($node, neu,old) {
					$node.text( neu );
				};
			}
		} 		
		return set;
	},
	transform = function( root, data, directive, state, tmplIndex){
		
		for(var i=0, l=directive.length; i<l; i++){
			var selSpec= directive[i][0], actionSpec= directive[i][1];
			
			// if( !selSpec.selector) then return 'undefined'; otherwise, if selSpec.selector is '.' return [root], and if it's not '.' return the nodes that match that selector.
			var nodes = selSpec.selector && selSpec.selector !== '.' ? $(selSpec.selector, root): [root],
				j = nodes.length, node, action, actionVal;

			if(j == 0)
				console.error( 'The selector "' + selSpec.selector + '" didn\'t match any node in the template:\n' + ( root.outerHTML || (root.tagName + root.className) ) );
					
			var value, actions= actionSpec.actions[tmplIndex];
				
			while(j--){
				node = nodes[ j ];
				actionVal= actionSpec.value;
				//if actionSpec is a loop, it has no 'value' property, unlike when actionSpec refers to an individula datamember
				if(actionVal === undefined){ 
					loopNode( node, actionSpec, data, state, tmplIndex );
				// if actionSpec is either a datamember (eg. 'mysel':'product.name'), a constant (string or number), or a function
				}else{
					action= actions[j];
					//DA: if actionSpec is not a function, ie. it's a constant (a string or number) or a datamember.
					if(typeof actionVal !== 'function'){
						//if actionVal is a constant, readData() returns 'undefined' (coz a datamember was not found), and the actionSpec value itself is used.
						//Warning: if readData() returns an empty array, (''+readData(   )) is evaluated as 'false' and thus it'd believe that actionVal is a constant instead of a datamember. That problem will also happen when a value in data is an empty string (ie. ""). However, in such cases the system shouldn't consider actionVal is a constant. how to fix it? don't use "||" to check readData() output, instead check if value is 'undefined'.  
						//value=	( '' + readData( actionVal, data ) ) || actionVal; // "|| actionVal" means: if no value found in data, actionSpec is just a constant, not a datamember name.
						value= readData( actionVal, data );
						if(value===undefined) value= actionVal; 
					}else{
						//if actionSpec is not a string, it's a function, so let's call it here. 
						value= actionVal.call( data.item || data, data.context? data: {context: data, node: node} )
						/*Important: In a loop, 'data' (in arg2) is an object like:
						data: {context: _full_data, <itemName>:  , items:  , item:  , pos:  , node:  }
						If not in a loop, pass to the func a similar object, with properties 'context' and 'node', but loop-based props ('items', 'item', and 'pos') will be undefined. The purpose is that all funcs receive the same data object, this allows to reuse a func in any part of a directive, whether the func is called inside a loop or not. */
					}
					// "action" is a func with two args: node and a string, which sets the value on the node (its text or attr). Warning: in the line below, the arg 'value' is not always a string, it might be a number, should i convert it to string? (ie. does action() need a string to set the value on the node? )
					action( $(node), value );
					
					// two-way databinding: bind data item and node
					state.push([node, selSpec,actionVal, data, action, db.bindDataItem]);
		
				}
			}
			
		}
		return root;
	};
	// for the elements IMG and INPUT it automatically binds the data to 'src' and 'value' attrs, instead of binding to the node's content. So, no need to define that the datamember binds to the node's attribute. This would make easier for developers to write directives, eg. for forms
	var autoAttrMap = {
			IMG:'src',
			INPUT:'value',
			SELECT:'value', // Not standard. Added to store the selected <option>s.
			FIELDSET: 'value' //Not standard
		};
	
	//AUTO INTERPOLATION: 
	//try to infer the mapping data to nodes by the semantic cues in HTML, such as 'id', 'class', and 'name' attrs
	function getAutoDirectives(node, data){
		var specList= [],
			openLoops=[];
			
		//TODO: if data is an array, i should create an spec for a loop	
		//eg. data= [{name:  }, {name:   }]	, eg. see file:///home/david/devel/docs/rendering/pure/docs/auto.iteration.htm, imagine the same example but without the property 'animals'. IMPORTANT: if data is array, then there could be a loop even if the attr matched is 'id' or 'name', ie. not only there can be a loop spec with 'class' attr as i initially thought. If there is a loop, what should be the node to clone (ie. 'sel' in selSpec)? an idea is to use the root node as the node to clone, so i'd have to use either its Id or class.
		/*if(Array.isArray(data)){
			data= data[0];
			curSpec= [];
			specList.push([{selector: ??????},{value:{loopSpec: ????? , directive: curSpec}}]);
			
		} */
			
		processNode(node, data, specList);	
				
		function processNode(n, data, curSpec, ctxSel, ctxData,loopId){
			var val, sel, prop, lastLoop;
			
			//Note: if there are nested open loops, at the moment i only check if the property is found in parent loop, I don't check if it's in other existing open loops. 
			
			//1. look for a matching 'id' attr
			if(n.id){
				val= readData(n.id, data);
				if(val!==undefined){ 
					prop= n.id; sel= '#'+prop;
				}
			}
			//2. look for a matching 'class' attr
			if( val===undefined && n.className ){
				var cs = (n.className.split)? n.className.split(' '):[];
				// for each className
				for(var i= 0, l=cs.length; i<l; i++){
					val= readData(cs[i],data);
					if(val!==undefined){ 
						prop= cs[i]; sel= '.'+prop;
						break;
					}				
				}
			}
			//3. look for a matching 'name' attr (for input elements)
			if(val===undefined && n.name ){
				val= readData(n.name, data);
				if(val!==undefined){
					prop= n.name; sel= '*[name='+prop+']';
				}
			}
			
			if(loopId!==undefined) lastLoop= openLoops[loopId]; //i check loopId is not 'undefined' because it could be 0, and in that case if(loopId) would return false, which is wrong.
			prop= (lastLoop)?lastLoop.i+'.'+prop: prop;
			
			if(val!==undefined){ 	
				if(lastLoop && n===lastLoop.target) sel='.'; //this is called when the previous node defined a loop, and thus the current node is the target node, ie. the one that is going to be cloned in each iteration. Thus, when rendering, the context of the selector will be the target node, and if i leave the orig selector (instead of replacing for '.') it won't be found, coz the context for finding the selector is the same node as the node that has to be found. that's why i have to use a selector '.' for this node. 
			}
			//TODO: if target node is a <select> or <fieldset>, the data asociated might be an array of values (ie. the values of the selected <option>s), so in such case don't treat it like a loop. Instead, create a directive that binds the entire array to the element. eg. in the if-check below, add the condition that node is not a <select> or <fieldset>, that way the else-block will be executed:
			// if(Array.isArray(val) && !/^(select|fieldset)$/.test(n.nodeName.toLowerCase())){
			if(Array.isArray(val) ){
				// if data value is an empty array, what to do? should i add loopDef to curSpec? I don't do so, coz loopDef.directive would be undefined, because data array is empty. So, i don't add any entry to curSpec[] for this case.
				if(!val.length) return;
				
				var cn= n.firstChild;
				while(cn){
					if(cn.nodeType===1) break;
					cn= cn.nextSibling;
				}
				var childSel;
				//note that we only check for its 'class' attr, not for its 'id', because this child node should not have an Id attr, coz the node is going to be cloned in the loop, and an Id must be unique.
				if(cn.className){ 
					childSel= '.'+cn.className.split(' ')[0];
				}else{
					//if child node has no class attr, set a custom attr to identify it
					var rand= Math.floor( Math.random() * 1000000 );
					cn.setAttribute('data-bind',rand);
					childSel= '[data-bind='+rand+']';
				}
				if(sel!=='.') sel += ' '+childSel;
				
				
				var loopDir=[]; 
				loopId= loopId? loopId+1: 0; //set new loop's ID
				openLoops.push({i: ''+loopId, data: val[0], target:cn}); // i'm assuming that loopId started at 0. 
								
				curSpec.push([{selector: sel}, {loopSpec: {itemName: ''+loopId , arrayName: prop} , directive: loopDir }]);
				curSpec= loopDir;
				data= val[0];
				//In arrayName, i prepend the parent loop's arrayItem to this loop's arrayName (if there is a parent loop). ie. if the parent loop's def is "mammal<- animals", the child loop definition is like "dog <- mammal.dogs". In my case, i use an index as itemName, ie. I use openLoop's length as the index
				
				//if val is an array of values (strings or numbers) instead of array of objects, we have to assign each value in array to the node that we are cloning in the loop. eg. if the node to clone is <div class="animals"></div>, and data= {animals:['dog','cat'] }, then loopSpec would be {itemName: '0', arrayName:'animals', directive:[   ]}, and 'directive' should be [ [{selector:'.'},{value: '0'}] ] to render each item in animals[] into the loop node (ie. <div class="animals">).   
				if(typeof data!== "object"){
					curSpec.push([{selector:'.'},{value: ''+loopId}]);
					return; //don't process child nodes
				}
			}else if(typeof val==="object"){
				data= val; //I do this to pass the nested obj to processNode(), to constraint the data scope of child nodes, instead of passing the full data obj
				ctxSel= sel;
				ctxData= prop;
				
			//Why do i check if val is not undefined? because readData() returns 'undefined' if no value was found in data, AND if a node has no 'id', 'class', and 'name' attrs then "val" is undefined too, because readData() was not even called. 
			}else if(val!==undefined){
				//Add the search scope to this selector. ie. if this node is inside a nested obj, prepend the selectors in this obj with the selector from parent element, to avoid matching this selector outside its scope. // only do it for nested objs, not for loops, coz loops define the target node, which already constraints the child selector. i pass new ctxSel to processNode() for nested objs.
				sel= (ctxSel)? ctxSel+' '+sel: sel;
				prop= (ctxData)? ctxData+'.'+prop: prop;
				curSpec.push([{selector: sel}, {value: prop}]);
			}
			
			//process child nodes
			var children= n.childNodes;
			for(var i=0, l=children.length; i<l; i++){
				if(children[i].nodeType === 1 )
					processNode(children[i], data, curSpec,ctxSel,ctxData, loopId);
			}
		}
		return specList;
	}
			
	// PUBLIC_METHODS
	
	var renderer= function(sel, ctxt){
		var i, targets;
		if(sel.nodeType)
			//if arg1 was a DOM node instead of a CSS selector 
			targets= [sel];
		else
			//find all template nodes for the given selector and context
			targets = $(sel, ctxt || document);
		
		i = this.length = targets.length;
		
		//fill an array of the template nodes
		while(i--){
			this[i] = targets[i];
		}
	};
	// compile the template with directive
	function compileSpec(root, specList){
		var selSpec, actionSpec, templData, autoAttr;
		for(var i=0, l=specList.length; i<l; i++){
			selSpec= specList[i][0]; actionSpec= specList[i][1]; templData=[];
			
			var nodes = selSpec.selector && selSpec.selector !== '.' ? $(selSpec.selector, root) : [root],
			j=nodes.length, node;
			
			while(j--){
				node= nodes[j];
				//actionSpec has no "value" property when it defines a loop, it only has a 'loopSpec' obj and a "directive" obj, which is a specList[]
				if(actionSpec.value === undefined){
					//if actionSpec is a loop, compile its specList[]
					compileSpec(node, actionSpec.directive ); 
				}else{
					//why do i check autoAttrs inside this 'else'? because it only makes sense when actionSpec is not a loop. eg. the user might want to render multiple <img> (or <input>) nodes and he'd define a loop with a target on that node (to clone it for each item in data array), so in that case we should not use autoAttrs because the user wants to target the node, not the attr. That's why we only try to infer autoAttrs when actionSpec is not a loop. FIXED: I check that "!selSpec.attr" before setting autoAttr, otherwise it could be overriding other attrs defined to be set on an <img>, <input>,...
					if(!selSpec.attr){
						autoAttr = autoAttrMap[node.tagName];
						if(autoAttr) selSpec.attr= autoAttr;
					}
					//get action and store it in this template's data. 
					templData.push(getAction(node, selSpec, actionSpec ));
				}
			}
			if(!actionSpec.actions) actionSpec.actions=[]; 
			actionSpec.actions.push(templData);
						
		}
	}
	
	// if data is passed to compile(), the auto-rendering is triggered automatically
	//TODO: [x] I should compile the directive as many times as templates are stored in 'this' (ie. in this[0]...). ie. an instance of this module stores an array of template nodes, in case multiple templates are defined for a directive. Since each of those templates might be different DOM tree, we need to create different compiled objects. And when we call render() or databind() we must select the right compiled object for each template. Implemented: I store the compiled data in actionSpec.actions, which is an array with as many items as templates in this; in compileSpec() i add a new [] for each template [ie. in two templates stored in our renderer the matched nodes might be different tagNames for the *same* selSpec, thats why i store an array with compiled data for each template]. Then, an item inside actionSpec.actions[] is again an Array, which holds the compiled data for each node matched in that template [what it stores is an action function, obtained from getAction(). Stores the compiled data for each node that matches the selSpec, because each matched node might be different tagName, so the function from getAction() could be different for each node even if selSpec is the same (eg. if selSpec is a class name it can match differenet tagNames), so we can't use the same action function for all nodes.]
	//Warning: if you call compile() with no directive and data, pass 'null' or 'undefined' in arg1; otherwise, it's not possible to detect when arg1 is a 'data' or 'directive' obj. 
	renderer.prototype.compile=function(directive, data){
		
		if(directive && !(directive instanceof Array))
			this.specList= directive= parse(directive);
		else
			this.specList= directive;
				
		//check if autoRender is true (ie. if compile() has 'data' in args2). If so: generate the autoRender directives from data and merge them with directives given in args.
		if(data){ 
			//TODO: getAutoDirectives() takes the template node in arg1. The problem is that we might have multiple template nodes (accessed with "this[i]"), and those templates might be different. So, i should calculate the autoDirectives for each template, and that will give us multiple specList. And in render() I'd have to render each template with its corresponding specList (thus, instead of storing "this.specList", I'd have to store an array of specLists: this.specList[]), ie. in render(), pass the corresponding specList[i] to transfom(   ). However, for testing the autorender feature, I haven't implemented those changed, and i just pass this[0] to getAutoDirectives(), ie. I take only the first template node.
			var autoDirs= getAutoDirectives(  this[0]  ,data);
			// merge them with directive in arg1, if it was given
			directive= this.specList=(directive)? directive.concat(autoDirs): autoDirs;
			
		}	
		//compile directives
		var i= this.length;
		while(i--){
			compileSpec(this[i], directive);
		}
		
		this.compiled= true;
		return this;//returns the renderer instance to be able to chain calls, eg. renderer= db('myselector').compile(  );
	}
	renderer.prototype.render=function(data, directive, autorender){
		
		if(!this.compiled){ 
			this.compile(directive, (autorender || !directive)? data: null); // only pass 'data' to compile() if autoRender is true, otherwise pass undefined. If 'autorender' was not passed in render()'s args but no directive was passed either, then assume that 'autorender' is true
		}
		directive= this.specList;
		
		var i = this.length, result=[], 
		state= db.newDbQueue(); //it's a queue used for data-binding 
		
		while(i--){
			result[i] = transform( this[i].cloneNode(true) , data, directive, state, i);
		}
		//Fire a custom event after finished rendering each view:
		//problem: Since nodes rendered in transform() are not in DOM, i can't fire events on them. Also, note that template nodes are not necessarily in DOM either. Ideally, i should fire the event on the view's root node, ie. the parent node in which the rendered content is going to be attached; but problem is I have no access to view's root. So, i fire an event on the document object and pass the rendered nodes. Any other solution?
		_.fire(document, "HTMLContentRendered",[result[0]]);
		
		this.state= state; // used when doing databinding. 
		
		return result;
	}
	
	//two_way - boolean, says whether data binding must be 1-way or 2-ways
	renderer.prototype.databind=function(two_way){
		
		this.state.processQueue();
	
		console.log("Done databind()");
		
	};
	db.setReadDataFn(readData);
	
	//this func is useful to access the data in an event handler, eg. if you have an HTML list of records, and attach an event to <ul> to handle delete a record (ie. use event delegation). If user clicks on remove button of an <li>, in the event handler I have access to the target node <li>, and use it to remove its associated datamember from the data, eg. a_tupla= getDataFromNode(my_li_node). For more info, read about "Unobstrusive event handling" in /docs/ui-design/unobstrusive-event-handling.htm
	//TODO: when a node is an item in a list, I can't get the data that refers to the whole node. eg. let's say i want to remove an <li> from a list, I'd need to get the data linked to that <li>, however now my code doesn't provide that, coz what getDataFromNode() returns is the data properties linked to that <li>'s content or attrs. I mean, in that example what I need is not "data[0][data[1]]", it's the object itself, ie. "data[0]". Solution: think of ALL use-cases for getDataFromNode(), and modify what it returns if necessary (ie. return the object itself instead of the object's properties)
	function getDataFromNode(node){
		
		var data= $.data(node, 'databind');
		if(!data) return null;
		
		//'data' is a tupla: (object, property). in order to be able to change/delete the property. Another option would be to return an object (instead of a tupla) like {object:   , property:   } 
		return data[0][data[1]];
		
	} 	
	
	//this func gets node(s) associated to given data spec
	/* Edit: I won't implement this func, because I don't see a use case. why need to get node associated to a datamember? a use case must be about data, not about appearance of the HTML. eg. highlight a node bound to a datamember is only about appearance, not about data handling.
	* 
	function getNodeFromData(object, property){	
	} */
	renderer.prototype.dataFor= getDataFromNode;
	
	return function(sel, ctxt){
		return new renderer(sel, ctxt);
	};
});
