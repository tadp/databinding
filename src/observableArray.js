// How to implement an array that extends the native Array? Answer:
// http://perfectionkills.com/how-ecmascript-5-still-does-not-allow-to-subclass-an-array/	

define(['events/bean'], function(_){

var pt= Array.prototype;

// In a function, 'arguments' object is an *array-like* object, not an *Array* object. So, I convert 'arguments' to an Array because apply() only supports Array as second argument in older browsers:
//https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/apply
function toArray(iterable) {
	return Array.prototype.slice.call(iterable);
} 
return{
	push: function() {
		var items = toArray(arguments);
		var result = Array.prototype.push.apply(this, items);
		_.fire(this,'add', [this.length - items.length, this.length - 1, items]);
		return result;
	},
	
	pop: function() {
		//if this array is empty, don't fire the 'remove' event
		if(!this.length) return;
		var result = Array.prototype.pop.call(this);
		_.fire(this,'remove', [this.length, this.length, [result]]);
		return result;
	},
	
	shift: function() {
		if(!this.length) return;
		var result = Array.prototype.shift.call(this);
		_.fire(this,'remove', [0, 0, [result]]);
		return result;
	},
	
	unshift: function() {
		var items = toArray(arguments);
		var result = Array.prototype.unshift.apply(this, items);
		_.fire(this,'add', [0, items.length, items]);
		return result;
	},
	
	splice: function(index, howmany, element1) {
		var args = toArray(arguments);
		var items = args.slice(2);
		var result = Array.prototype.splice.apply(this, args);
		
		//Fix: items could be an array, and splice() inserts its elements into our array, instead of inserting the full array as one element. So, check for such case:
		if(Array.isArray(items) && items.length===1)
			items= items[0];
		
		if (result.length) {
			_.fire(this,'remove', [index, result.length - 1, result]);
		}
		if (items.length) {
			_.fire(this,'add', [index, items.length - 1, items]);
		}
		return result;
	},
	
	sort: function() {
		var args = toArray(arguments);
		var result = Array.prototype.sort.apply(this, args);
		_.fire(this,'reset', [0, this.length - 1, this]);
		return result;
	},
	
	reverse: function() {
		var result = Array.prototype.reverse.call(this);
		_.fire(this,'reset', [0, this.length - 1, this]);
		return result;
	}
}
});
