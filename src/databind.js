define(['jquery','lang/aug','./observableArray','./observableProperty','events/bean'],function($,aug, oArray, oProp, _){
	
	var DEFAULT_CHANGE_EVENT='change',	
		readData=function(){};
		
	function bindDataItem(node, selSpec, actionSpec, data, action){
			
		//if actionSpec is typeof *function* or a constant (string or number), I can't bind data to node, coz the constant is not stored in the data, it's not meant to change.
		//First, check that actionSpec is a string, not a function or number 
		if(typeof actionSpec != 'string')	return;
		
		//Second, check that actionSpec is not a constant string or is not in given data. 
		if(!readData( actionSpec, data )) return;	//[returns '' when actionSpec not found in data]	
		
		
		//Warning: the 'arguments' object is not really an Array, it's an array-like object. Don't use array-like objects to pass to apply(), because it's not supported by all browsers (eg. IE9, Chrome 14). To make an array from your 'arguments' object, you can use the Array.prototype.slice method (see below). Refs:
		//https://developer.mozilla.org/en-US/docs/JavaScript/Reference/Global_Objects/Function/apply
		//http://stackoverflow.com/questions/1881905/call-function-with-multiple-arguments 
		var args = Array.prototype.slice.call(arguments);
		
		//Detect if data is 1)an array of values (primitive types), 2)an array of objects, or 3) a single object (not an array). I need to detect case (1) because that'll be handled different than if it's an object.
		var isValuesArray=false, v = data[actionSpec];
		if(v && v.item) isValuesArray=true;
		args.push(isValuesArray);

		//Explanation about code above:
		//if "v" is undefined: it's an array of objects or nested objects (..or a property not found in data), ie. actionSpec is like "product.price", so data[actionSpec] is not found. 
		// if "v" is defined, it's either (3) [a plain object (not an array)] or (1) [an array of values]. Let's analise them with the following directive:
		/*	{ '#mymsg':'message',
			  '#taglist':{
				 't <- tags':{
				    '.':'t'
				 }
			  }
			} 	*/	
		//(3) look at the first rule, in which actionSpec is "message". data[actionSpec] will return a value (ie. "v" is not null), because 'message' is a property of data, which is an object.
		//(1) look at the second rule. The data is an array stored in property 'tags', eg. tags=['one','two']. actionSpec is 't', used to refer to each item in array. So, data['t'] will return 'loopCtxt' obj, whose property "item" gives the value for "t" (the itemName), which is a single value, not an object.
		//(2) if it's array of objects, actionSpec says the itemName and property, eg. "product.price". 'product' is the itemName. data['product'] returns loopCtxt obj, whose "item" property gives an object (and that object will have a property "price"), not a single value.
				
		bindToNode.apply(null, args);
		bindToData.apply(null, args);	
	}
	function bindToNode(node, selSpec, actionSpec, data, action, isValuesArray){
		
		// if a node's text is editable, and selSpec says 'append'/prepend, i should not bind the data to node. why? if we have to append/prepend a new value to the node, we have to do it on the original text of node, ie. we have to replace the old value that was appended/prepended before on the original text; however, when a text can be edited by user it's difficult to replace the old value with the new value, coz the user might have deleted it, or deleted part of it, and we can't find it. solution: don't bind data to node when the directive says to append/prepend to text in node IF the node can be edited by user (ie. <input type=text >, <textarea>, and any node with contentEditable=true (edit: actually, any node can be made contentEditable=true at a time after databinding, so the rule is to not bind data to node for ANY text node (eg. <div>,<p>, <span>, <h*>, etc ) when selSpec says append/prepend, independently of its contentEditable attr )) 
		if(selSpec.prepend || selSpec.append){
			var tag= node.nodeName;
			//if target is "value" attr in <input type="text"> or <textarea>
			if((tag=="input" && node.type=="text") || tag=="textarea")
				if(selSpec.attr=="value") return;
			//if target is node's content, not node's attr
			if(!selSpec.attr)	 return;
		}	
		
		//register the callback that will be called by the listener set on the datamember
		var callback= function(property, oldValue, newValue, obj){ 
			if(node !== obj.__trigger__)
			action($(node), newValue, oldValue); //'action' is the set() that changes node's content or attr
		};
		
		// if data is an array_of_values instead of array_of_objects, i can't bind on a property change. Instead, i should bind on the *set* operation on the array, ie. myArray[item_index]= new_value
		if(isValuesArray){	// bind to array's 'set' operation. 
			//var array= data[actionSpec].items;
			_.on(data[actionSpec].items, 'set', callback);
			
		}else{ //bind to property change, ie. observableProperty.bind( ... )
			//readData() when arg3 is 'true' it returns a tupla instead of the datamember's value. The tupla is (object, property), in order to be able to change/delete the datamember.
			var member= readData(actionSpec, data, true);
			oProp.bind(member[0],member[1], callback); //arg1 is the object, arg2 is the property
		}
	}
	function bindToData(node, selSpec, actionSpec, data, action, isValuesArray){
		//Called if actionSpec is a string, not a loop or function.
		
		//Note: This func adds the data-binding attrs to bind node to datamember, so when 'change' event is fired (or 'blur' for contentEditable), a callback will get the target node, read its data-binding attrs, and set the value of the corresponding datamember, using writeData()
		//Note2: don't attach the properties to DOM nodes via 'expando' properties. if you only store strings, not objects, then it's no problem to use expando properties. If you store objects, it'd be better to use an approach similar to jQuery's $.data(), to avoid memory leaks in some browsers (eg. IE).
		
		//only bind a node to data when it's a property that users can change in the UI, ie. if node is <input> (depends on its type), <select>, <textarea>, or any node that is contentEditable:
		switch(node.nodeName.toLowerCase()){
			case "input":
				var type= node.type;
				if(type=="checkbox" || type=="radio")
					if(selSpec.attr!="checked") return;
				else if(/^(hidden|button|submit|reset)$/.test(type) || selSpec.attr!="value") 
					return;
				break;
			case "option":
				if(selSpec.attr != "selected") return; 
				break;
			case "textarea": case "select": case "fieldset":
				if(selSpec.attr != "value") return;
				break; 
			default:
				// if node is not any of previous tags, there are 2 cases: 
				//1) if target is a node's attr, don't bind to data, coz users can't change such attr (Users can only change attrs in form elements, eg. 'value' or 'checked'). 
				//2) if target is a node's content, do bind to data coz the node might be made contentEditable=true, so if user edits the text the bound data will be updated.
				if(selSpec.attr) return;
		}	
		
		// approach: stores a tuple: (object, property_name), ie. the tuple has 1)the object that contains the property and 2) the property name to access its value. this way, i avoid the problem of changes in the path to the object, eg. in "products.4.prices.2" if a product at index 2 is removed from products[], then we'd need to update the index in the path of all affected nodes (ie. change to "products.3.prices.2" in this node), but that can be un-managable! so, a better approach is to not store the path as string; and instead, just store a pointer to the object that has the bound property, this way if an item in products[] is removed I won't need to update anything, coz the pointer to our object doesn't change. implementation:
		//$.data(node, 'databind', readData(actionSpec, data, true));
		//Edit: the line above doesn't take into account arrays of values, only arrays of objects. When data is an array of values, we should store the tuple (array, item_index), but readData() returns only the item value in this case, so I handle here the two different cases [see readData() to understand this code]:
		var tuple;
		if(isValuesArray){
				var v= data[actionSpec];
				tuple= [v.items, v.pos];
		}else{
				tuple= readData(actionSpec, data, true);
		}
		$.data(node, 'databind', tuple);					
	
	}
	
	function processQueue(){
		//in state[], i store arrays which have the callback and arguments to pass to the callback. i put the callback in the last item in the array, coz it's faster to remove than if i put it in the firt position in array. so i get the callback with state[j].pop(). At that point, the items left in array state[j] are only the arguments for the callback, so i use apply() to call the callback, and its arg2 takes the array with the args to pass it. ///why do i add the callback to the end of state[] array, instead of position 0?? because it's faster to extract the callback if it's the last item in array, ie. pop() is faster than shift(), see http://stackoverflow.com/questions/6501160/why-is-pop-faster-than-shift

		//WARNING: in apply(), in arg1 I pass 'null', it works only for funcs not attached to an object. eg. using 'null' will fail to call the callback "console.log", because the method log() is attached to the object "console", so we should pass the object "console" as arg1 in apply(). solution?: don't know... anyway, the only callbacks I have of this type are Bean.on(  ) (ie. to bind events for add/remove items in a data array); to fix it I added a function tempEvtBind(), which serves as 'proxy' to call Bean.on() 
		for(var i=0, l=this.length; i<l; i++){
			var args= this[i];
			args.pop().apply(null, args);
		}
		
		//clear. There is no "clear()" method for Array! In JS, the correct way to "clear" an array is setting its length=0, and it'll automatically delete all items with index greater than length value. see http://stackoverflow.com/questions/1232040/how-to-empty-an-array-in-javascript
		this.length=0;
	}

	function writeData(obj, property, value, node){
		//when setting the datamember's value, it'll trigger a change event on the datamember, but we must avoid that the change event updates the node that triggered such change; otherwise we would enter an infinite loop of updates between DOM node and datamember. [note that we can't stop the change event on the datamember, because there might be other nodes that should be updated upon a change on the datamember; we just need to avoid that the datamember's change event does not update the *trigger* node of the change. how? I save a pointer to the node in the property __trigger__, and the event callback (see bindToNode()) will check that the node to update is not the same as in __trigger__ ]
		obj.__trigger__= node;
		obj[property]= value;
		obj.__trigger__= undefined;
	}
	
	// Attach 'change' event handlers for present AND future element (ie. using event delegation)
	function attachChangeEvents() {
		//NOTES: In a <select>, in the 'change' event, event.target is the <select> node, not the <option> that the user clicked on. <option> nodes can't fire a 'change' event.
		
		//TODO: bind change (or blur?) event handler for contentEditable elements, eg.:
		/* $(document).delegate('*[contenteditable=true]', DEFAULT_CHANGE_EVENT, function (e){ 
			var nodeData= $.data(this, 'databind');
			if(!nodeData) return;
			writeData(nodeData[0], nodeData[1], $(this).html());
		}
		*/
		// for <select>, it's only possible to bind to <select> element, not to the <option> elements, because updating data of <option> elements is complex, since you need to update both deselected and selected options.
		$(document).delegate('select', DEFAULT_CHANGE_EVENT, function(e){
			var nodeData= $.data(this, 'databind'),
				newVal;
			
			
			
			//TODO: do input validation. If not validated, return. eg.:
			//if(!validated(this, nodeName, inputType)) return; //where the function 'validated' is from other module, which is imported in this module's dependencies. Note, maybe i should move the declaration of 'nodeData' (see above) after this line, coz if input fails validation we wasted time searching for $.data(node,   )
			
			
			
			if(!nodeData) return;
			//newVal= $(this).val(); //returns an array with the selected values. But I don't use jQuery's val() to avoid dependency on jQuery in this module
				
			if(!this.multiple){
				newVal= this.value;
			}else{
				var opts= this.options; newVal= [];
				for(var i=0, l=opts.length; i< l; i++)
					opts[i].selected && newVal.push(opts[i].value);
			}
			writeData(nodeData[0], nodeData[1], newVal, this);
		});
		
		// Attach event for simple databinding
		$(document).delegate('input, textarea', DEFAULT_CHANGE_EVENT, function (e) {
			var nodeName= this.nodeName, inputType= this.type, 
				nodeData= $.data(this, 'databind');
			
			//If the modified input is not valid, do not update the bound data. I use HTML5 Constraint Validation API for validating input. Such functionality is in the module /baselibs/lang/validation.js
			if(this.checkValidity && !this.checkValidity) return;
			
			//TODO: do input validation. If not validated, return. eg.:
			//if(!validated(this, nodeName, inputType)) return; //where the function 'validated' is from other module (put that module in this same folder?), which is imported in this module's dependencies. Note, maybe i should move the declaration of 'nodeData' (see above) after this line, coz if input fails validation we wasted time searching for $.data(node,   )
			
			
			
			
			if(inputType=='checkbox' || inputType=='radio'){
				var parent= this.parentNode;
				if(parent && parent.nodeName=="FIELDSET"){
					var fsData= $.data(parent, 'databind');
				
				if(fsData){
					//build an array with the values of checked elements. This is the same as what I do with a <select> node
					var opts= $('input', parent), newVal=[];
					for(var i=0, l= opts.length; i<l; i++)
						opts[i].checked && newVal.push(opts[i].value);
						
					writeData(fsData[0], fsData[1], newVal, parent);
				}
				}
				if(nodeData){
					//if it's a radio button in a group, the 'change' event fires only for the new selected node. So, here update the bound data of the deselected node. I can't know which node was deselected, thus I have to loop thru all nodes in the group.
					if(inputType=="radio"){
						var group= $("input[name="+ this.name+"]");
						if(group.length>1)
							for(var i=0, l= group.length; i<l; i++){
								if(group[i]===this) continue;
								var radioData= $.data(group[i], 'databind');
								radioData && writeData(radioData[0], radioData[1], false, group[i]);
							}
					}
					//if the node that changed state has also a bound datamember, I must update its value in addition to the fieldset's bound datamember
					writeData(nodeData[0], nodeData[1], this.checked, this); //'this.checked' returns true/false. With jQuery, $(this).attr('checked') returns 'checked' when true, and 'undefined' when false.
				}
				return;
			}
			
			if(!nodeData) return; //it means this node is not bound to data.
			
			//In other input's type (eg. text, date, etc) or textarea, what changes is the 'value' property
			writeData(nodeData[0], nodeData[1], this.value, this);		
				
			
			//if it's a 'select', it could be bound to the individual <option>s or the <select>. if bound to <select>, use jQuery's val() to get the selected options (it can be 1 value or an array if <select multiselect=true>). if bound to individual options, it's more complex, coz it needs to get the <option> node (or nodes) that was selected and update its associated datamember AND update the associated datamembers of the previoulsy selected <option>s.
			
			//if it's checkbox or radiobutton, it's similar to select list, ie. i detect if it was bound on the group of checkboxes/radiobuttons or on an individual checkbox/radiobutton, and update the associated datamember. problem: how to detect if it was bound to a *group* of checkbox/radiobuttons? checkboxes/radionbuttons don't have a parent node, like with <select>, so I can't attach a binding to that node. radios are grouped by using the same 'name' attr. idea1: wrap the group inputs with a <fieldset> tag and bind the datamember properties to that node (TODO: if a <fieldset> does not exist around the group inputs, it should be automatically added at binding time).

			//TODO: trigger pre 'change' event. This is useful for input *validation*. The event is triggered on the data member, not on the DOM element. how get the datamember?: 
			//var target= $.data(node, 'databind'); //a tupla: (data_obj, property)
			//ev.trigger( target[0], 'pre-change', {bounddata: target, value: newVal, node: this});

			//TODO: trigger post 'change' event. This is useful to bind to changes on datamembers, instead of changes on DOM nodes, eg. instead of binding to an input textbox, I can bind to the associated data. Advantage: in JS code I don't need to *hardcode* the selector used to bind to the DOM element's; instead, I take advantage of the definitions in the PURE's directives, which already define the CSS selector of the textbox that is to be bound to a datamember; so if later I change the selector of the textbox, my JS code is not affected because it's bound to change event on the data, so I don't need to modify my JS code, in that way i make event binding *unobstrusive*.  Edit: However, there are cases where I can't do that and I need to use a CSS selector to bind to an HTML node, eg. to set a click evt handler on a button, or to highlight a textbox when onfocus evt (see notes on getNodeFromData()), etc
			//ev.trigger(target[0], 'change', {bounddata: target, value: newVal, node: this});

			//the 'pre-change' and 'post-change' events could be fired in writeData(), which is where I update the bound data. Warning: for the 'pre-change' event, don't change the bound data until all the event listeners finish and none of them returned "false"; thus, i need to capture the return value of the event callbacks, how do I do that in Bean?? ie. simply using Bean.fire(obj, 'pre-change', {  }) won't work as i need, then how can i do it? it's not possible, see http://stackoverflow.com/questions/3142724/  In jQuery, there is a func that does what i need: triggerHandler() [http://api.jquery.com/triggerHandler/], I can port it to Bean.js. What does triggerHandler() do? i think it gets the array of callbacks attached to an object for an event, and loops to call the callbacks, this allows 2 things: 1) the event callbacks are executed in *synchronous* way (instead of asynchronous), and 2) we can get the callbacks' return value. 
			
			//Edit: Maybe we don't need a pre-change and post-change events(?). To do validation of input, the developer should use the HTML5 Constraint Validation API, so our code will check validity of input automatically. And for post-change, it's already possible to bind to a property change, using observableProperty.js module, eg. oProp.bind(object,property, callback);

		});
		
		
	}
	
	//NOTE: attachChangeEvents() must run only once, not on each view, because we attach events in the 'document' node, not on the view's root node (why not on the view's root node? because it's unnecessary to add the listeners to each view, we add only one listener to the 'document' node)
	attachChangeEvents();

	return{
		watchArray: function(items){
			aug(items,oArray);
		},
		tempEvtBind: function(ob, event, cb){
			_.on(ob, event, cb);
		},
		newDbQueue: function(){
			var q=[];
			q.processQueue= processQueue;
			return q;
		},
		setReadDataFn: function(fn){
			readData= fn;
		},
		bindDataItem: bindDataItem
	}
});
