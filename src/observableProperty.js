//Code is based on PropertyChange class (binding.js) of Simpli5 project 
define([], function(){

//TODO: in makeObservable(), it uses defineProperty(), which is a standard, but not supported by all browsers. Thus, I must add fallbacks for IE, Opera,... see /docs/data-binding/ for discussion.

	var PropertyChange= {
	bind: function(obj, property, observer) {
		var props = property.split(/\s*,\s*/);
		var properties = obj.__observers__;
		if (!properties) {
			obj.__observers__ = properties = {};
		}
		
		for (var i = 0, l = props.length; i < l; i++) {
			property = props[i];
			if (!this.isObservable(obj, property)) {
				this.makeObservable(obj, property);
			}
			var observers = properties[property];
			if (!observers) {
				properties[property] = observers = [];
			}
			if (observers.indexOf(observer) == -1) observers.push(observer);
		}
	},
	
	unbind: function(obj, property, observer) {
		var props = property.split(/\s*,\s*/);
		var properties = obj.__observers__;
		if (!properties) return;
		
		for (var i = 0, l = props.length; i < l; i++) {
			property = props[i];
			var observers = properties[property];
			if (!observers) continue;
			var index = observers.indexOf(observer);
			observers.splice(index, 1);
		}
	},
	
	dispatch: function(obj, property, oldValue, newValue){
		if (oldValue === newValue) return;
		var properties = obj.__observers__, i, l;
		if (!properties) return;
		
		var observers = [].concat(properties[property] || []).concat(properties['*'] || []);
		for (i = 0, l = observers.length; i < l; i++) {
			observers[i](property, oldValue, newValue, obj);
		}
	},
	
	isObservable: function(obj, property) {
		//An approach is to check if this property has a setter already, however this procedure is not supported in ancient browsers. So an easier approach is to check for observers[property]:
		return !!obj.__observers__[property]; //true/false
	},
	//NOTE: when I define a property, it overrides any existing property with same name. So if I define a setter but not a getter, I won't be able to access its value (ie. obj.property returns undefined). Thus, I need to define both get/set for my case.
	makeObservable: function(obj, property) {		
		var prop = obj[property];
 
		Object.defineProperty(obj, property, {
     		get: function() { return prop;},
     		set: function(value) { 
				//this.seconds = value * 1000;
				var oldValue = prop;
				if (oldValue == value) return;
				prop = value;
				PropertyChange.dispatch(obj, property, oldValue, value); 
				//'this' inside this function refers to obj, not to our module's object, so I named it as PropertyChange, to access dispatch() inside this func.
			}
		});
				
		// to detect if a property is observable already
		//obj.__lookupSetter__(property).observable = true;
	
	}
}
	return PropertyChange;
});
