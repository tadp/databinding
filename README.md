# Data-binding system
Pure **unobtrusive** approach to bind data and HTML views.

##Motivation
The aim is to create a rendering system that meets the following requirements:  
 
*   No foreign concepts such as <%=foo%> or {{foo}} in HTML content.
*   Auto-binding mode to automatically interpolate data to template without need of directives.
*   Promote the separation of concerns principle by decoupling presentation from data.
*   Promote portable code/markup by decoupling HTML views from data models.
*   Make both the markup and code more readable and maintainable.
*   Allow designers to write up sample markup and test styling without a developer.
*   Increase maintainability by developers with various skill sets.
   
   
Separation of HTML and data aspects that allows for iteration and flexibility when designs change. This is specially important when different teams (designers and developers) work on the views.

Allows mock data within the template (for testing) that is replaced at runtime. This way designers can test the HTML views in a browser without rendering data on them. 


##Auto-binding data and HTML
It can automatically bind data to DOM elements, by using semantic cues in the HTML, such as 'id', 'class', and 'name' attributes. The idea is to re-use semantic information already in the HTML, without the need to add anything.

It's good practice to define semantic attrs in HTML. eg. a class "telephone" instead of style-based attrs. So, we can take advantage of this information already in the HTML. This saves having to write directives in a lot of cases. 

###Automatic Binding values

TODO

###Rendering data items in an array

TODO


##Using directives to define bindings
There are cases you want to define how to bind the data to DOM, this can be done using JSON-like directives. That is, there is complete separation between HTML and data bindings.

A directive defines bindings using CSS-selectors and actions. In keys, you use CSS-selectors to define the target element(s) to bind to. In right side, you define the operations to perform on the target(s). The simplest is a single value, which says the datamember to bind to. 

    directive= { 
	'.username': 'user.username',
	'.telephone': 'user.phone',
    }
 

You can combine auto-binding and directives together. Argument3 in render() is a boolean value to say whether enable auto-render or not (default:false).

TODO

##Usage

#### module(selector|node [, context]):  
This function returns an instance of a renderer. It takes the following arguments: 
 
* selector: a CSS-selector for the template(s) in the HTML
* context: a DOM node that defines the context for the CSS selector search
* Instead, it's possible to pass a DOM node to use as HTML template .

#### render(data [, directive, autorender]):   
It returns an array with the rendered HTML (as DOM nodes). It takes these arguments:  

* data:       the JS/JSON data to be rendered
* directives:  optional. Directives to bind data and HTML elements. Used when the template was not compiled before
* autorender:  optional. Boolean value to auto-bind data and HTML. Used when the template was not compiled before.   

Note it returns a list of DOM nodes, not a single one. That's because the renderer instance might have multiple templates, as the match of the CSS selector passed to create the instance. 

#### compile(directive[, data]):
Optional. It pre-compiles the template. Calling render() will compile the template if it's not compiled.
  
* directive: pass 'null'(or undefined) if you want to use auto-binding
* data: optional. If data is provided, it automatically uses auto-binding. If a directive was also provided, the system will combine auto-binding and the definitions in the directive. 


"Hello World" example:   

HTML template   
    `<div class="container">`  
        `<p class="message"></p>`  
    `</div>`  

JSON Data    
    {  
      "message": "Hello World!!"  
    }  
    
    var renderer= r('.container').render(data);


##Tests
[http://tadp.bitbucket.org/databinding/tests/templates1.html](http://tadp.bitbucket.org/databinding/tests/templates1.html)
